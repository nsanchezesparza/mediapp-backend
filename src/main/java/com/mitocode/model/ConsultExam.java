package com.mitocode.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "consulta_examen")
@IdClass(ConsultExamPK.class)
public class ConsultExam {
	@Id
	private Exam examen;
	
	@Id
	private Consult consulta;

	
	
	
	public Exam getExamen() {
		return examen;
	}
	public void setExamen(Exam examen) {
		this.examen = examen;
	}
	public Consult getConsulta() {
		return consulta;
	}
	public void setConsulta(Consult consulta) {
		this.consulta = consulta;
	}

	
}

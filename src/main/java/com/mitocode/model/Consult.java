package com.mitocode.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name = "consultas")
public class Consult {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idConsult;

	@JsonSerialize(using = ToStringSerializer.class)
	@Column(name = "fecha_hora")
	private LocalDateTime dateTime;

//	Relaciones
	@ManyToOne
	@JoinColumn(name = "id_patient", nullable = false)
	private Patient patient;

	@ManyToOne
	@JoinColumn(name = "id_doctor", nullable = false)
	private Doctor doctor;

	@ManyToOne
	@JoinColumn(name = "id_especialty", nullable = false)
	private Specialty specialty;

	//Se genera esta relacion xq en una consulta se guarda detalles de consultas q son listas
	//el objtivo de jpa es hacer una representacion de como es la base de dato y como la data se guardara
	@OneToMany(mappedBy = "consult", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<DetailConsult> detailConsult;

	public int getIdConsult() {
		return idConsult;
	}

	public void setIdConsult(int idConsult) {
		this.idConsult = idConsult;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Specialty getSpecialty() {
		return specialty;
	}

	public void setSpecialty(Specialty specialty) {
		this.specialty = specialty;
	}

	public List<DetailConsult> getDetailConsult() {
		return detailConsult;
	}

	public void setDetailConsult(List<DetailConsult> detailConsult) {
		this.detailConsult = detailConsult;
	}
	
	
}

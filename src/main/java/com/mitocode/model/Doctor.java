package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medicos")
public class Doctor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDoctor;

	@Column(name = "nombres", nullable = false, length = 70)
	private String names;

	@Column(name = "apellidos", nullable = false, length = 70)
	private String fullNames;

	@Column(name = "CMP", nullable = false, length = 12)
	private String cmp;

	public Doctor() {

	}

	public Doctor(String names, String fullNames, String cmp) {
		super();
		this.names = names;
		this.fullNames = fullNames;
		this.cmp = cmp;
	}

	public int getIdDoctor() {
		return idDoctor;
	}

	public void setIdDoctor(int idDoctor) {
		this.idDoctor = idDoctor;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getFullNames() {
		return fullNames;
	}

	public void setFullNames(String fullNames) {
		this.fullNames = fullNames;
	}

	public String getCmp() {
		return cmp;
	}

	public void setCmp(String cmp) {
		this.cmp = cmp;
	}

}

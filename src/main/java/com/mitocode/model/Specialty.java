package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "especialidades")
public class Specialty {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEspecialty;

	@Column(name = "nombre", nullable = false, length = 50)
	private String name;
	
	public Specialty() {
	
	}

	public Specialty(String name) {
		super();
		this.name = name;
	}

	public int getIdEspecialty() {
		return idEspecialty;
	}

	public void setIdEspecialty(int idEspecialty) {
		this.idEspecialty = idEspecialty;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	

}

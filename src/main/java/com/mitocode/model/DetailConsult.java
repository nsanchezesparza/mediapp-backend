package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "detalle_consultas")
public class DetailConsult {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDetailConsult;

	@Column(name = "diagnostico", nullable = false, length = 70)
	private String diagnoctic;

	@Column(name = "tratamiento", nullable = false, length = 300)
	private String treatment;
	
//	Relaciones
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_consult",nullable = false)
	private Consult consult;

	public int getIdDetailConsult() {
		return idDetailConsult;
	}

	public void setIdDetailConsult(int idDetailConsult) {
		this.idDetailConsult = idDetailConsult;
	}

	public String getDiagnoctic() {
		return diagnoctic;
	}

	public void setDiagnoctic(String diagnoctic) {
		this.diagnoctic = diagnoctic;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public Consult getConsult() {
		return consult;
	}

	public void setConsult(Consult consult) {
		this.consult = consult;
	}
	
	
}

package com.mitocode.service;

import java.util.List;

public interface ICRUD<T> {

	T registrar(T entity);

	T modificar(T entity);

	void eliminar(T entity);

	T listarId(int id);

	List<T> listar();
}

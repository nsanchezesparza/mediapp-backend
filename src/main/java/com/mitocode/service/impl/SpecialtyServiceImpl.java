package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISpecialtyDAO;
import com.mitocode.model.Specialty;
import com.mitocode.service.ISpecialtyService;

@Service
public class SpecialtyServiceImpl implements ISpecialtyService {

	@Autowired
	ISpecialtyDAO dao;

	@Override
	public void registrar(Specialty especialidad) {
		dao.save(especialidad);

	}

	@Override
	public void modificar(Specialty especialidad) {
		dao.save(especialidad);

	}

	@Override
	public void eliminar(Specialty especialidad) {
		dao.delete(especialidad);

	}

	@Override
	public Specialty listarId(int idEspecialidad) {

		return dao.findById(idEspecialidad).orElse(null);
	}

	@Override
	public List<Specialty> listar() {

		return dao.findAll();
	}

}

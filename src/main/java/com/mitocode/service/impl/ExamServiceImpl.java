package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IExamDAO;
import com.mitocode.model.Exam;
import com.mitocode.service.IExamService;

@Service
public class ExamServiceImpl implements IExamService {

	@Autowired
	private IExamDAO examDao;

	@Override
	public void registrar(Exam exam) {
		examDao.save(exam);

	}

	@Override
	public void modificar(Exam exam) {
		examDao.save(exam);

	}

	@Override
	public void eliminar(Exam exam) {
		examDao.delete(exam);

	}

	@Override
	public Exam listarId(int idExam) {
		return examDao.findById(idExam).orElse(null);
	}

	@Override
	public List<Exam> listar() {

		return examDao.findAll();
	}

}

package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IPatientDAO;
import com.mitocode.model.Patient;
import com.mitocode.service.IPatientService;

@Service
public class PatientServiceImpl implements IPatientService{

	@Autowired
	IPatientDAO dao;
	
	@Override
	public Patient registrar(Patient entity) {

		return dao.save(entity);
	}

	@Override
	public Patient modificar(Patient entity) {

		return dao.save(entity);
	}

	@Override
	public void eliminar(Patient entity) {
		dao.delete(entity);
		
	}

	@Override
	public Patient listarId(int id) {

		return dao.findById(id).orElse(null);
	}

	@Override
	public List<Patient> listar() {
		
		return dao.findAll();
	}

}

package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IDoctorDAO;
import com.mitocode.model.Doctor;
import com.mitocode.service.IDoctorService;

@Service
public class DoctorServiceImpl implements IDoctorService {

	@Autowired
	IDoctorDAO dao;

	@Override
	public Doctor registrar(Doctor entity) {

		return dao.save(entity);
	}

	@Override
	public Doctor modificar(Doctor entity) {

		return dao.save(entity);
	}

	@Override
	public void eliminar(Doctor entity) {
		dao.delete(entity);

	}

	@Override
	public Doctor listarId(int id) {

		return dao.findById(id).orElse(null);
	}

	@Override
	public List<Doctor> listar() {

		return dao.findAll();
	}

}

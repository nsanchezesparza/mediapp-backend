package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.dao.IConsultDAO;
import com.mitocode.dao.IConsultExamDAO;
import com.mitocode.dto.ConsultListExamDTO;
import com.mitocode.model.Consult;
import com.mitocode.service.IConsultService;

@Service
public class ConsultServiceImpl implements IConsultService {

	@Autowired
	private IConsultDAO dao;

	@Autowired
	private IConsultExamDAO ceDAO;

	@Transactional
	@Override
	public Consult registrar(ConsultListExamDTO consultDTO) {
//		consultDTO.getDetailConsult().forEach(x -> x.setConsult(consult));
//		return dao.save(consultDTO);

		Consult cons = new Consult();
		consultDTO.getConsult().getDetailConsult().forEach(x -> x.setConsult(consultDTO.getConsult()));
		cons = dao.save(consultDTO.getConsult());

		consultDTO.getListExam().forEach(e -> ceDAO.registrar(consultDTO.getConsult().getIdConsult(), e.getIdExam()));
		return cons;
	}

	@Override
	public void modificar(Consult consult) {
		dao.save(consult);

	}

	@Override
	public void eliminar(Consult consult) {
		dao.delete(consult);

	}

	@Override
	public Consult listarId(int idConsult) {

		return dao.findById(idConsult).orElse(null);
	}

	@Override
	public List<Consult> listar() {
		
		return dao.findAll();
	}

}

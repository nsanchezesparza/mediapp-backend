package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Exam;

public interface IExamService {

	void registrar(Exam exam);

	void modificar(Exam exam);

	void eliminar(Exam exam);

	Exam listarId(int idExam);

	List<Exam> listar();
}

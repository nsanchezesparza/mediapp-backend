package com.mitocode.service;

import java.util.List;

import com.mitocode.dto.ConsultListExamDTO;
import com.mitocode.model.Consult;

public interface IConsultService {

	Consult registrar(ConsultListExamDTO consultDTO);

	void modificar(Consult consult);

	void eliminar(Consult consult);

	Consult listarId(int idConsult);

	List<Consult> listar();
}

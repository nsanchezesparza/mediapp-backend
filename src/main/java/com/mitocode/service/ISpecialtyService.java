package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Specialty;

public interface ISpecialtyService {

	void registrar(Specialty especialidad);

	void modificar(Specialty  especialidad);

	void eliminar(Specialty  especialidad);

	Specialty listarId(int idEspecialidad);

	List<Specialty > listar();
}

package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Consult;

@Repository
public interface IConsultDAO extends JpaRepository<Consult, Integer> {

}

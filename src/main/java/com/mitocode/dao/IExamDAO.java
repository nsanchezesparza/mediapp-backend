package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Exam;
@Repository
public interface IExamDAO extends JpaRepository<Exam, Integer>{

}

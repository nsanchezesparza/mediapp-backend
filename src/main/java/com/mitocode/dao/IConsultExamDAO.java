package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;

import com.mitocode.model.ConsultExam;

@Repository
public interface IConsultExamDAO extends JpaRepository<ConsultExam, Integer>{

//	@Transactional
	@Modifying
	@Query(value = "INSERT INTO consulta_examen(id_consult,id_exam) values(:idConsult, :idExam)", nativeQuery = true)
	int registrar(@Param("idConsult") Integer idConsult, @Param("idExam")Integer IdExam);
}

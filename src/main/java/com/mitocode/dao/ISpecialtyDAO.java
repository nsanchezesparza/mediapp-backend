package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Specialty;

@Repository
public interface ISpecialtyDAO extends JpaRepository<Specialty, Integer> {

}

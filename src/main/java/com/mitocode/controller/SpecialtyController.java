package com.mitocode.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Specialty;
import com.mitocode.service.ISpecialtyService;

@RestController
@RequestMapping("/especialidades")
public class SpecialtyController {

	@Autowired
	private ISpecialtyService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Specialty>> listar() {
		List<Specialty> specialtyes = new ArrayList<>();
		specialtyes = service.listar();
		return new ResponseEntity<List<Specialty>>(specialtyes, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Specialty> listarId(@PathVariable("id") Integer id) {
		Specialty specialty = new Specialty();
		specialty = service.listarId(id);
		if (specialty == null) {
			throw new ModeloNotFoundException("ID: "+id);
		}
		return new ResponseEntity<Specialty>(specialty, HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@Valid @RequestBody Specialty specialty) {
		service.registrar(specialty);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(specialty.getIdEspecialty()).toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@Valid @RequestBody Specialty specialty) {
		service.modificar(specialty);
		return new ResponseEntity<Object>(HttpStatus.OK);		
	}

	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		
		Specialty obj = service.listarId(id);
		if (obj == null) {
			throw new ModeloNotFoundException("ID: "+id);
		}else {
			service.eliminar(obj);
		}
		
	}

}

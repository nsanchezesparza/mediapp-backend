package com.mitocode.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.dto.ConsultListExamDTO;
import com.mitocode.dto.ConsultaDTO;
import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Consult;
import com.mitocode.service.IConsultService;

@RestController
@RequestMapping("/consultas")
public class ConsultController {

	@Autowired
	private IConsultService service;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Consult>> listar() {
		List<Consult> consultas = new ArrayList<>();
		consultas = service.listar();
		return new ResponseEntity<List<Consult>>(consultas, HttpStatus.OK);
	}

	@GetMapping(value = "/hateoas", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ConsultaDTO> listarHateoas() {
		List<Consult> consultas = new ArrayList<>();
		List<ConsultaDTO> consultasDTO = new ArrayList<>();
		consultas = service.listar();

		for (Consult c : consultas) {
			ConsultaDTO d = new ConsultaDTO();
			d.setIdConsulta(c.getIdConsult());
			d.setMedico(c.getDoctor());
			d.setPaciente(c.getPatient());

			ControllerLinkBuilder linkTo = linkTo(methodOn(ConsultController.class).listarId((c.getIdConsult())));
			d.add(linkTo.withSelfRel());
			consultasDTO.add(d);

			ControllerLinkBuilder linkTo1 = linkTo(
					methodOn(PatientController.class).listarId((c.getPatient().getIdPatient())));
			d.add(linkTo1.withSelfRel());
			consultasDTO.add(d);

			ControllerLinkBuilder linkTo2 = linkTo(
					methodOn(DoctorController.class).listarId((c.getDoctor().getIdDoctor())));
			d.add(linkTo2.withSelfRel());
			consultasDTO.add(d);
		}
		return consultasDTO;
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Consult> listarId(@PathVariable("id") Integer id) {
		Consult obj = new Consult();
		obj = service.listarId(id);
		if (obj == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Consult>(obj, HttpStatus.OK);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@Valid @RequestBody ConsultListExamDTO consultaDTO) {
		Consult obj = new Consult();

		obj = service.registrar(consultaDTO);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdConsult())
				.toUri();
		return ResponseEntity.created(location).build();
	}

}

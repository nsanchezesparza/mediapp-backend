package com.mitocode.dto;

import java.util.List;

import com.mitocode.model.Consult;
import com.mitocode.model.Exam;

public class ConsultListExamDTO {

	private Consult consult;
	private List<Exam> listExam;
	
	
	
	public Consult getConsult() {
		return consult;
	}
	public void setConsult(Consult consult) {
		this.consult = consult;
	}
	public List<Exam> getListExam() {
		return listExam;
	}
	public void setListExam(List<Exam> listExam) {
		this.listExam = listExam;
	}
	
	
}

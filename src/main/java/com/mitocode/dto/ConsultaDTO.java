package com.mitocode.dto;

import org.springframework.hateoas.ResourceSupport;

import com.mitocode.model.Doctor;
import com.mitocode.model.Patient;

public class ConsultaDTO extends ResourceSupport {

	private int idConsulta;
	private Doctor medico;
	private Patient paciente;

	public int getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(int idConsulta) {
		this.idConsulta = idConsulta;
	}

	public Doctor getMedico() {
		return medico;
	}

	public void setMedico(Doctor medico) {
		this.medico = medico;
	}

	public Patient getPaciente() {
		return paciente;
	}

	public void setPaciente(Patient paciente) {
		this.paciente = paciente;
	}

}
